'use strict'

const fs = require('fs')
const path = require('path')
const uuid = require('uuid/v4')
const BPromise = require('bluebird')
const NodeCache = require('node-cache')
const safeParse = BPromise.method(JSON.parse)

const isNil = require('lodash.isnil')
const hasProp = require('lodash.has')
const isError = require('lodash.iserror')
const isEmpty = require('lodash.isempty')
const isString = require('lodash.isstring')
const isNumber = require('lodash.isnumber')
const isPlainObject = require('lodash.isplainobject')

const Prom = require('../prometheus-logger')
const LatencyMonitor = require('../prometheus-latency')
const Broker = require('../broker.lib')

class Gateway extends LatencyMonitor {
  constructor () {
    super()
    this.config = {}
    this.port = process.env.PORT || 8080

    const BROKER = process.env.BROKER
    const ACCOUNT = process.env.ACCOUNT
    const PLUGIN_ID = process.env.PLUGIN_ID

    const LOGGERS = `${process.env.LOGGERS || ''}`.split(',').filter(Boolean)
    const OUTPUT_PIPES = `${process.env.OUTPUT_PIPES || ''}`.split(',').filter(Boolean)
    const COMMAND_RELAYS = `${process.env.COMMAND_RELAYS || ''}`.split(',').filter(Boolean)
    const EXCEPTION_LOGGERS = `${process.env.EXCEPTION_LOGGERS || ''}`.split(',').filter(Boolean)

    let _broker = new Broker()
    let _prometheus = new Prom()
    let _cmdCache = new NodeCache({ stdTTL: 60 * 5 })

    process.env.PORT = undefined
    process.env.BROKER = undefined
    process.env.ACCOUNT = undefined
    process.env.LOGGERS = undefined
    process.env.PLUGIN_ID = undefined
    process.env.OUTPUT_PIPES = undefined
    process.env.COMMAND_RELAYS = undefined
    process.env.EXCEPTION_LOGGERS = undefined

    this.pipe = (data, sequenceId) => {
      if (isEmpty(data) || !isPlainObject(data)) return BPromise.reject(new Error('Invalid data received. Data should be and Object and should not empty.'))
      if (!isEmpty(sequenceId) && !isString(sequenceId)) return BPromise.reject(new Error('Kindly specify a valid sequence id'))

      let readInfo = {
        account: ACCOUNT,
        deviceId: data.rkhDeviceInfo._id,
        dateTime: Date.now()
      }

      if (sequenceId) {
        return _broker.queues['data'].publish({
          plugin: PLUGIN_ID,
          pipe: OUTPUT_PIPES,
          sequenceId: sequenceId,
          data: data
        }).then(() => {
          return _broker.queues['device.reading'].publish(readInfo)
        })
      } else {
        return BPromise.each(OUTPUT_PIPES, (pipe) => {
          data.lastReadingDateTime = Date.now()
          return _broker.queues[pipe].publish(data)
        }).then(() => {
          return _broker.queues['device.reading'].publish(readInfo)
        })
      }
    }

    this.relayCommand = (command, devices, deviceGroups, source = '') => {
      if (!command) return BPromise.reject(new Error('Kindly specify the command/message to send'))
      if (!isString(command)) return BPromise.reject(new Error('Command must be a valid string'))
      if (isEmpty(devices) && isEmpty(deviceGroups)) return BPromise.reject(new Error('Kindly specify the target device types or devices'))

      if (!(isString(deviceGroups) || Array.isArray(deviceGroups)) || !(isString(devices) || Array.isArray(devices))) {
        return BPromise.reject(new Error(`'deviceGroups' and 'devices' must be a string or an array.`))
      }

      let sequenceId = uuid()

      return BPromise.each(COMMAND_RELAYS, cmdRelay => {
        return _broker.queues[cmdRelay].publish({
          account: ACCOUNT,
          pluginId: PLUGIN_ID,
          command: command,
          sequenceId: sequenceId,
          deviceGroups: deviceGroups,
          devices: devices,
          source: source
        })
      })
    }

    this.sendCommandResponse = (commandId, response) => {
      if (!commandId || !isString(commandId)) return BPromise.reject(new Error('Kindly specify the command id'))
      if (!response || !isString(response)) return BPromise.reject(new Error('Kindly specify the response'))

      return _broker.queues['cmd.responses'].publish({
        commandId: commandId,
        response: response
      })
    }

    this.notifyConnection = (deviceId) => {
      if (!deviceId || !isString(deviceId)) return BPromise.reject(new Error('Kindly specify a valid device identifier'))

      return BPromise.all([
        _broker.queues['devices'].publish({
          operation: 'connect',
          account: ACCOUNT,
          device: {
            _id: deviceId
          }
        }),
        new BPromise((resolve, reject) => {
          let requestId = uuid()

          if (!COMMAND_RELAYS.length) return resolve()

          _broker.rpcs['cmd.pending.rpc'].once(requestId, (cmds) => {
            safeParse(cmds.content.toString() || '{}').then(arrCmds => {
              return BPromise.each(arrCmds, (cmd) => {
                delete cmd.sequenceId
                this.emit('command', cmd)
              })
            }).then(resolve).catch(reject)
          })

          _broker.rpcs['cmd.pending.rpc'].publish(requestId, {
            account: ACCOUNT,
            device: {
              _id: deviceId,
              requestId: requestId,
              cmdRelays: COMMAND_RELAYS
            }
          }).catch(reject)
        }).timeout(10000, 'Request for pending command has timed out')
      ])
    }

    this.notifyDisconnection = (deviceId) => {
      if (!deviceId || !isString(deviceId)) return BPromise.reject(new Error('Kindly specify a valid device identifier'))

      return _broker.queues['devices'].publish({
        operation: 'disconnect',
        account: ACCOUNT,
        device: {
          _id: deviceId
        }
      })
    }

    this.requestDeviceInfo = (deviceId) => {
      return new BPromise((resolve, reject) => {
        if (!deviceId || !isString(deviceId)) return reject(new Error('Kindly specify a valid device identifier'))

        let requestId = uuid()

        _broker.rpcs['device.info.rpc'].once(requestId, (deviceInfo) => {
          safeParse(deviceInfo.content.toString() || '{}').then(resolve).catch(reject)
        })

        _broker.rpcs['device.info.rpc'].publish(requestId, {
          account: ACCOUNT,
          device: {
            _id: deviceId,
            requestId: requestId
          }
        }).catch(reject)
      }).timeout(10000, 'Request for device information has timed out.')
    }

    this.syncDevice = (deviceInfo, deviceGroup = '') => {
      if (!deviceInfo || !isPlainObject(deviceInfo)) {
        return BPromise.reject(new Error('Kindly specify a valid device information/details'))
      }

      if (!(hasProp(deviceInfo, '_id') || hasProp(deviceInfo, 'id'))) {
        return BPromise.reject(new Error('Kindly specify a valid id for the device'))
      }

      if (!hasProp(deviceInfo, 'name')) {
        return BPromise.reject(new Error('Kindly specify a valid name for the device'))
      }

      return _broker.queues['devices'].publish({
        operation: 'sync',
        account: ACCOUNT,
        data: {
          group: deviceGroup,
          device: deviceInfo
        }
      })
    }

    this.removeDevice = (deviceId) => {
      if (!deviceId || !isString(deviceId)) {
        return BPromise.reject(new Error('Kindly specify a valid device identifier'))
      }

      return _broker.queues['devices'].publish({
        operation: 'remove',
        account: ACCOUNT,
        device: {
          _id: deviceId
        }
      })
    }

    this.setState = (state) => {
      if (isNil(state) || (isString(state) && isEmpty(state))) {
        return BPromise.reject(new Error(`Please specify a valid state to set.`))
      }

      return _broker.queues['plugin.state'].publish({
        state: state,
        plugin: PLUGIN_ID
      })
    }

    this.getState = () => {
      return new BPromise((resolve, reject) => {
        let requestId = uuid()
        _broker.rpcs['plugin.state.rpc'].once(requestId, (state) => {
          resolve(state.content.toString())
        })
        _broker.rpcs['plugin.state.rpc'].publish(requestId, {
          plugin: PLUGIN_ID
        }).catch(reject)
      }).timeout(10000, 'Request for plugin state has timed out.')
    }

    this.setDeviceState = (deviceId, state) => {
      return new BPromise((resolve, reject) => {
        let requestId = uuid()

        if (!deviceId || !isString(deviceId)) return reject(new Error('Kindly specify a valid device identifier'))
        if (isNil(state)) return reject(new Error('Kindly specify the device state'))

        _broker.rpcs['device.state.rpc'].once(requestId, deviceInfo => {
          resolve(deviceInfo)
        })

        _broker.rpcs['device.state.rpc'].publish(requestId, {
          account: ACCOUNT,
          device: {
            _id: deviceId,
            state: state
          }
        }).catch(reject)
      }).timeout(10000, 'Setting device state has timed out.')
    }

    this.setDeviceLocation = (deviceId, lat, long) => {
      if (!deviceId || !isString(deviceId)) {
        return BPromise.reject(new Error('Kindly specify a valid device identifier'))
      }

      if (!isNumber(lat) || !isNumber(long)) {
        return BPromise.reject(new Error(`Kindly specify a valid 'lat, long' coordinates`))
      }

      return _broker.queues['device.location'].publish({
        account: ACCOUNT,
        device: deviceId,
        long: long,
        lat: lat
      })
    }

    this.log = (logData) => {
      if (isEmpty(logData)) return BPromise.reject(new Error(`Please specify a data to log.`))
      if (!isPlainObject(logData) && !isString(logData)) return BPromise.reject(new Error('Log data must be a string or object'))

      return BPromise.all([
        BPromise.each(LOGGERS, logger => {
          return _broker.queues[logger].publish(logData)
        }),
        _broker.queues['logs'].publish({
          account: ACCOUNT,
          pluginId: PLUGIN_ID,
          type: 'Gateway',
          data: logData
        })
      ])
    }

    this.logException = (err) => {
      _prometheus.error()
      if (!isError(err)) return BPromise.reject(new Error('Please specify a valid error to log.'))

      let errData = {
        name: err.name,
        message: err.message,
        stack: err.stack
      }

      return BPromise.all([
        BPromise.each(EXCEPTION_LOGGERS, exceptionLogger => {
          return _broker.queues[exceptionLogger].publish(errData)
        }),
        _broker.queues['exceptions'].publish({
          account: ACCOUNT,
          pluginId: PLUGIN_ID,
          type: 'Gateway',
          data: errData
        })
      ])
    }

    safeParse(process.env.CONFIG || '{}').then(config => {
      this.config = config
      process.env.CONFIG = undefined

      return BPromise.resolve()
    }).then(() => {
      return _broker.connect(BROKER)
    }).then(() => {
      let root = process.cwd()
      let home = path.join(root, 'keys')

      if (!fs.existsSync(home)) fs.mkdirSync(home)

      let writeFile = (filePath, content) => {
        if (isEmpty(content)) return BPromise.resolve('')

        return fs.writeFileAsync(filePath, content).then(() => {
          return BPromise.resolve(filePath)
        })
      }

      return BPromise.props({
        rootCrl: writeFile(path.join(home, 'root-crl.pem'), process.env.ROOT_CRL),
        ca: writeFile(path.join(home, 'server-ca.pem'), process.env.CA),
        crl: writeFile(path.join(home, 'server-crl.pem'), process.env.CRL),
        key: writeFile(path.join(home, 'server-key.pem'), process.env.KEY),
        cert: writeFile(path.join(home, 'server-cert.pem'), process.env.CERT)
      }).then(keys => {
        Object.assign(this, keys)

        process.env.ROOT_CRL = undefined
        process.env.CA = undefined
        process.env.CRL = undefined
        process.env.KEY = undefined
        process.env.CERT = undefined

        return BPromise.resolve()
      })
    }).then(() => {
      let genericQueues = [
        'data',
        'logs',
        'devices',
        'exceptions',
        'cmd.responses',
        'device.reading',
        'device.location',
        'plugin.state'
      ]

      let queueIds = genericQueues
        .concat(EXCEPTION_LOGGERS)
        .concat(COMMAND_RELAYS)
        .concat(OUTPUT_PIPES)
        .concat(LOGGERS)

      return BPromise.each(queueIds, queueId => {
        return _broker.createQueue(queueId)
      })
    }).then(() => {
      return BPromise.each(COMMAND_RELAYS, (cmdRelay) => {
        return _broker.createExchange(`${cmdRelay}.${PLUGIN_ID}`, `${cmdRelay}.*`).then((exchange) => {
          return exchange.consume((msg) => {
            if (!isEmpty(msg)) {
              safeParse(msg.content.toString() || '{}').then(cmd => {
                if (isEmpty(_cmdCache.get(`${cmd.device}:${cmd.sequenceId}`))) {
                  _cmdCache.set(`${cmd.device}:${cmd.sequenceId}`, '0')

                  delete cmd.sequenceId
                  return this.emit('command', cmd)
                }
              }).catch(err => {
                console.error(err)
              })
            }
          })
        })
      })
    }).then(() => {
      return BPromise.each(['device.info.rpc', 'device.state.rpc', 'plugin.state.rpc', 'cmd.pending.rpc'], (rpcQueueName) => {
        return _broker.createRPC('client', rpcQueueName).then((queue) => {
          return queue.consume()
        })
      })
    }).then(() => {
      process.nextTick(() => {
        require('../../http-server/prom-http-server')
        this.emit('ready')
      })

      return BPromise.resolve()
    }).catch(err => {
      console.error(err)
      throw err
    })
  }
}

module.exports = Gateway
