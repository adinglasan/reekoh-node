'use strict'

let url = require('url')
const amqp = require('amqplib')
const BPromise = require('bluebird')

const isEmpty = require('lodash.isempty')
const isPlainObject = require('lodash.isplainobject')

const EventEmitter = require('events').EventEmitter
const rkhLogger = require('./reekoh.logger')

class Queue {
  constructor (channel, queueName) {
    this.channel = channel
    this.queueName = queueName
  }

  publish (data, options) {
    let channel = this.channel
    let queueName = this.queueName

    if (isPlainObject(data) || Array.isArray(data)) data = JSON.stringify(data)

    if (isPlainObject(options)) {
      Object.assign(options, {
        persistent: false
      })
    } else if (isEmpty(options)) {
      options = {
        persistent: false
      }
    }

    return new BPromise((resolve, reject) => {
      if (!channel.sendToQueue(queueName, Buffer.from(data, 'utf8'), options)) {
        reject(new Error('Queue.publish() failed!'))
      } else {
        resolve()
      }
    })
  }

  consume (handler) {
    let channel = this.channel
    let queueName = this.queueName

    return channel.consume(queueName, (msg) => {
      if (!isEmpty(msg)) {
        handler(msg)
      }

      channel.ack(msg)
    })
  }
}

class RpcQueue extends EventEmitter {
  constructor (channel, queueName, generatedQueueName) {
    super()

    this.channel = channel
    this.queueName = queueName
    this.generatedQueueName = generatedQueueName
  }

  // used for testing only. rpc server should be in platform
  serverConsume (promise) {
    let channel = this.channel
    let queueName = this.queueName

    return channel.consume(queueName, (msg) => {
      if (isEmpty(msg)) return

      promise(msg).then((response) => {
        if (!isEmpty(response)) {
          let data = Buffer.from(response.toString(), 'utf8')

          let options = {
            correlationId: msg.properties.correlationId
          }

          channel.sendToQueue(msg.properties.replyTo, data, options)
        }

        channel.ack(msg)
      }).catch((err) => {
        rkhLogger.error(err)
      })
    })
  }

  publish (requestId, data) {
    let channel = this.channel
    let queueName = this.queueName
    let generatedQueueName = this.generatedQueueName

    return new BPromise((resolve, reject) => {
      if (isPlainObject(data) || Array.isArray(data)) data = JSON.stringify(data)

      let isPublished = channel.sendToQueue(queueName, Buffer.from(data, 'utf8'), {
        correlationId: requestId,
        replyTo: generatedQueueName,
        persistent: false
      })

      if (!isPublished) {
        reject(new Error('Queue.publish() failed!'))
      } else {
        resolve()
      }
    })
  }

  consume () {
    let channel = this.channel
    let generatedQueueName = this.generatedQueueName

    let answer = (msg) => {
      this.emit(msg.properties.correlationId, msg)
    }

    return channel.consume(generatedQueueName, answer, { noAck: true })
  }
}

class Broker {
  constructor () {
    this.channel = null
    this.connection = null
    this.queues = {}
    this.exchanges = {}
    this.rpcs = {}
  }

  connect (connString) {
    let parsedUrl = url.parse(connString)
    let connOpts = {}

    if (parsedUrl.protocol === 'amqps:') {
      connOpts = {
        servername: parsedUrl.hostname
      }
    }

    return amqp.connect(connString, connOpts).then((conn) => {
      this.connection = conn
      return conn.createChannel()
    }).then((channel) => {
      this.channel = channel

      this.channel.prefetch(50)

      this.channel.on('close', (msg) => {
        rkhLogger.error(msg)
        throw new Error(`AMQP channel emitted a 'close' event! - please check your AMQP server.`)
      })

      this.channel.on('error', (err) => {
        rkhLogger.error(err)
        throw new Error(`AMQP channel emitted an 'error' event! - please check your AMQP server.`)
      })

      return BPromise.resolve(channel)
    })
  }

  createQueue (queueName) {
    return this.channel.assertQueue(queueName).then(() => {
      let queue = new Queue(this.channel, queueName)

      this.queues[queueName] = queue

      return BPromise.resolve(queue)
    })
  }

  createExchange (exchangeName, routingKey) {
    return this.channel.assertExchange('amq.topic', 'topic', routingKey || exchangeName).then(() => {
      return this.channel.assertQueue(exchangeName)
    }).then((ret) => {
      return this.channel.bindQueue(ret.queue, 'amq.topic', routingKey || exchangeName)
    }).then(() => {
      let exchange = new Queue(this.channel, exchangeName)

      this.exchanges[exchangeName] = exchange

      return BPromise.resolve(exchange)
    })
  }

  createRPC (type, queueName) {
    let queueOptions = {}

    if (type === 'server') {
      queueOptions = {
        durable: false
      }
    } else {
      queueOptions = {
        exclusive: true
      }
    }

    return this.channel.assertQueue((type === 'server' ? queueName : ''), queueOptions).then((ret) => {
      if (type === 'server') this.channel.prefetch(1)

      return BPromise.resolve((type !== 'server') ? ret.queue : queueName)
    }).then((retQueue) => {
      let rpc = new RpcQueue(this.channel, queueName, retQueue)

      this.rpcs[queueName] = rpc

      return BPromise.resolve(rpc)
    })
  }
}

module.exports = Broker
